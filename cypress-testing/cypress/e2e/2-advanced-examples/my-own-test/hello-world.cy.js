// / <reference types="cypress" />

// const { before } = require("cypress/types/lodash");
// const { beforeEach } = require("mocha");

// describe("Basic Tests", () => {
//   it("Every basic element exists", () => {
//     cy.visit("https://codedamn.com");
//     cy.contains("Learn Programming").should("exist");
//   });
// });

// it.only("Creat Account", () => {
//   cy.viewport(1280, 720);
//   cy.visit("https://codedamn.com");
//   cy.contains("Sign in").click();
//   cy.contains("Create a free account").click();
//   cy.contains("Or use email to signup").should("exist");
//   cy.get('[data-testid="name"]').focus().should("be.enabled").type("iyey");
//   cy.get('[data-testid="username"]')
//     .focus()
//     .should("be.enabled")
//     .type("iyeyputri");
//   cy.get('[data-testid="email"]')
//     .focus()
//     .should("be.enabled")
//     .type("putriiyey17@gmail.com");
//   cy.get('[data-testid="password"]')
//     .focus()
//     .should("be.enabled")
//     .type("NAsigoreng10");
//   cy.contains("submit details").should("exist");
//   cy.contains('[data-testid="submit-btn"]').click({ force: true });
// });

// it.only("Page Login ", () => {
//   cy.viewport(1280, 720);
//   cy.visit("https://codedamn.com");
//   cy.contains("Sign in").click();
//   cy.contains("Sign in to codedamn").should("exist");
//   cy.contains("Forgot your password?").should("exist");
//   cy.contains("Don't have an account?").should("exist");
// });

// it.only("The login page links work", () => {
//   cy.viewport(1280, 720);
//   cy.visit("https://codedamn.com");
//   cy.contains("Name").should("exist");
//   cy.contains("Username").should("exist");
//   cy.contains("Email Address").should("exist");
//   cy.contains("Password").should("exist");
// });

// it.only("Cannot login with invalid data", () => {
//   cy.viewport(1280, 720);
//   cy.visit("https://codedamn.com");
//   cy.get('[href="/login"]').click();
//   cy.contains("Sign in to codedamn").should("exist");
//   cy.get('[data-testid="username"]')
//     .focus()
//     .should("be.enabled")
//     .type("adminadmin");
//   cy.get('[data-testid="password"]')
//     .focus()
//     .should("be.enabled")
//     .type("Nasigoreng19");
//   cy.get('[data-testid="login"]').click({ force: true });
//   cy.contains("Unable to authorize").should("exist");
// });

// it.only("Cannot login with invalid data", () => {
//   cy.viewport(1280, 720);
//   cy.visit("https://codedamn.com");
//   cy.get('[href="/login"]').click();
//   cy.contains("Sign in to codedamn").should("exist");
//   cy.get('[data-testid="username"]')
//     .focus()
//     .should("be.enabled")
//     .type("adminadmin");
//   cy.get('[data-testid="password"]')
//     .focus()
//     .should("be.enabled")
//     .type("Nasigoreng19");
//   cy.get('[data-testid="login"]').click({ force: true });
//   cy.contains("Unable to authorize").should("exist");
// });

// // it('Every basic element exists on mobile', () => {
//     cy.viewport('iphone-x')
//     cy.visit('https://codedamn.com')
// })

// it.only("Login should work fine", () => {
//   cy.viewport(1280, 720);
//   cy.visit("https://codedamn.com");
//   cy.get('[href="/login"]').click();
//   cy.contains("Sign in to codedamn").should("exist");
//   cy.get('[data-testid="username"]')
//     .focus()
//     .should("be.enabled")
//     .type("iyeyputri");
//   cy.get('[data-testid="password"]')
//     .focus()
//     .should("be.enabled")
//     .type("NAsigoreng10");
//   cy.get('[data-testid="login"]').click({ force: true });
//   cy.contains("iyey").should("exist");
// });

describe("Basic Desktop", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });

  it("Login should work fine", () => {
    //todo: set this as localstorage token for authentication
    const token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Iml5ZXlwdXRyaSIsIl9pZCI6IjYzMzBmMmIxYjgzNjQ2MDAwOWE0MGRjYiIsIm5hbWUiOiJpeWV5IiwiaWF0IjoxNjY0NDQ4NDUwLCJleHAiOjE2Njk2MzI0NTB9.z9g7eyNsDdg8YwRMeN5kyOxR00UNf-UWPkPbjhBR8HA";

    cy.get('[href="/login"]').click();
    cy.contains("Sign in to codedamn").should("exist");
    cy.get('[data-testid="username"]')
      .focus()
      .should("be.enabled")
      .type("iyeyputri");
    cy.get('[data-testid="password"]')
      .focus()
      .should("be.enabled")
      .type("NAsigoreng10");
    cy.get('[data-testid="login"]').click({ force: true });
    cy.url().should("include", "/dashboard");
  });
});
