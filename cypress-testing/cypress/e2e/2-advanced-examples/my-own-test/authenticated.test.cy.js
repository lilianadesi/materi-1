/// <reference types="cypress" />
const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Iml5ZXlwdXRyaSIsIl9pZCI6IjYzMzBmMmIxYjgzNjQ2MDAwOWE0MGRjYiIsIm5hbWUiOiJpeWV5IiwiaWF0IjoxNjY0NDQ4NDUwLCJleHAiOjE2Njk2MzI0NTB9.z9g7eyNsDdg8YwRMeN5kyOxR00UNf-UWPkPbjhBR8HA";

describe("Authenticated test", () => {
  before(() => {
    cy.then(() => {
      window.localStorage.setItem("__auth__token", token);
    });
  });
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });

  it("Should load playground correctly", () => {
    cy.visit("https://codedamn.com/playgrounds");
    cy.contains("Playgrounds", { timeout: 10 * 1000 }).should("exist");
  });
});
